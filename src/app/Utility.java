package app;

//helper class
//hunnik abimeetodeid
public class Utility {
	
	static boolean isInteger(String s) {
	    try { 
	        Integer.parseInt(s); 
	    } catch(NumberFormatException e) { 
	        return false; 
	    }
	    // only got here if we didn't return false
	    return true;
	}

	static boolean validWordLen(String length) {
		if (isInteger(length)) {
			int intLength = Integer.parseInt(length);
			if(0 < intLength && intLength < 21) {	//pikemaid s�nu on m�ttetult v�he
				return true;
			}
		}
		return false;
	}
	
	static boolean validGuessNr(String guesses) {
		if (isInteger(guesses)) {
			int guessNr = Integer.parseInt(guesses);
			if (0 < guessNr && guessNr < 26) {	//26 arvamisega kindel v�it, 25ga arvab ka enamsti �ra
				return true;
			}
		}
		return false;
	}
	
	static boolean isLetter(String input) {
		if (input.length() == 1 && Character.isLetter(input.charAt(0))) {
			return true;
		}
		return false;
	}
}
