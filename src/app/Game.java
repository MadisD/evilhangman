package app;

import java.util.HashSet;

//v�ljade uuendamis loogika v�iks siin olla vb..

public class Game {

	private int WordLength;
	private boolean ShowRemainingWords;
	private int GuessesLeft;
	private String LettersUsed;
	private String GuessedPart; //string representing already guessed letters and their positions
	private HashSet<String> Words; 
	
	
	public Game(int wordLength, boolean showRemainingWords, int guessesLeft,
			String guessedPart, String lettersUsed, HashSet<String> words) {
		WordLength = wordLength;
		ShowRemainingWords = showRemainingWords;
		GuessesLeft = guessesLeft;
		LettersUsed = lettersUsed;
		GuessedPart = guessedPart;
		Words = words;
	}
	
	public void setGuessesLeft(int guessesLeft) {
		GuessesLeft = guessesLeft;
	}

	public void subtractGuess() {
		this.GuessesLeft -= 1;
	}

	
	
	public String getGuessedPart() {
		return GuessedPart;
	}

	public void setGuessedPart(String guessedPart) {
		GuessedPart = guessedPart;
	}

	public HashSet<String> getWords() {
		return Words;
	}

	public void setWords(HashSet<String> words) {
		this.Words = words;
	}

	public String getLettersUsed() {
		return LettersUsed;
	}

	public void setLettersUsed(String lettersUsed) {
		LettersUsed = lettersUsed;
	}

	public int getWordLength() {
		return WordLength;
	}

	public boolean isShowRemainingWords() {
		return ShowRemainingWords;
	}

	public int getGuessesLeft() {
		return GuessesLeft;
	}
}
