package app;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

public class Main {

	static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		boolean kasUus = true;
		
		while (kasUus) {
			initializeGame();
			
			//kogu m�ng m�ngitud
			System.out.println("Would you like to play again?(yes/no)");
			if (sc.nextLine().equals("no")) {
				kasUus = false;
				sc.close();
			}
		}
		
	}

	private static void initializeGame() {
		//algus
		//kogub info game instance loomiseks
		System.out.println("HANGMAN"); //no need to tell them it's evil
		System.out.println("What length word would you like to play with? (1-21)");
		
		String WordLengthSTR = sc.nextLine();
		

		while (! Utility.validWordLen(WordLengthSTR)) {
			System.out.println("Input invalid. Try again: ");
			WordLengthSTR = sc.nextLine();
		}
		int  WordLength = Integer.parseInt(WordLengthSTR); 
		
		System.out.println("How many guesses would you like to have? (1-25)");
		String guesses = sc.nextLine();
		while (! Utility.validGuessNr(guesses)) {
			System.out.println("Input invalid. Try again: ");
			guesses = sc.nextLine();
		}
		int Guesses = Integer.parseInt(guesses);
		
		boolean ShowRemainingWords = false;
		System.out.println("Would you like to see the number of possible remaining words? (yes/no)");
		if (sc.nextLine().equals("yes")) {
			ShowRemainingWords = true;
		}
		
		
		
		//soovitud pikkusega s�nade lugemine
		HashSet<String> words = readWords(WordLength);
		
		//algne guessed word representation
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < WordLength; i++) {
			sb.append("_");
		}
		String GuessedWord = sb.toString();
		
		//initial LettersUsed as empty string
		String LettersUsed = "";
		
		Game game = new Game(WordLength, ShowRemainingWords, Guesses, GuessedWord, LettersUsed, words);
		
		play(game);
	}

	private static void play(Game game) {
		
		//game loop
		while(game.getGuessesLeft() > 0) {
			System.out.println("Guesses left: " + game.getGuessesLeft());
			System.out.println("Used letters: " + game.getLettersUsed());
			System.out.println("Word: " + game.getGuessedPart());
			
			if (game.isShowRemainingWords()) {
				System.out.println("Number of words remaining: " + game.getWords().size());
			}
			
			System.out.println("Enter guess:");
			String playersGuess = sc.nextLine().toLowerCase();
			
			while (! Utility.isLetter(playersGuess)) {
				System.out.println("Input invalid. Try again: ");
				playersGuess = sc.nextLine().toLowerCase();
			}

			if (rightGuess(playersGuess, game)) {
				//
				System.out.println("You guessed right.");
				
				//check win
				if (! game.getGuessedPart().contains("_")) {
					System.out.println("You win!");
					System.out.println("The word was: " + getRandWord(game));
					game.setGuessesLeft(0);
				}
			}
			else {
				//
				System.out.println("Sorry, you guessed wrong.");
				
				//check loss
				if (game.getGuessesLeft() == 0) {
					String word = getRandWord(game);
					System.out.println("You lose! The word was: " + word);
					
				}
			}
		}
		
	}

	private static boolean rightGuess(String playersGuess, Game game) {
		//kui suurimas families on playersGuess siis tagastab true
				//ja muudab uueks p�hilistiks
				//ja updateib currentWord jms
		
		
		HashMap<String, HashSet<String>> families = 
				divideIntoFamilies(playersGuess, game);
		
		String biggestFamPatt = findMax(families);
		
		//trickery bc set has no get function
		for (Iterator<String> it = families.get(biggestFamPatt).iterator(); it.hasNext(); ) {
			String word = it.next();
			if (word.contains(playersGuess)) {
				
				//------
				//should be a separate update function?
				game.setWords(families.get(biggestFamPatt)); //uuendab s�nade seti
				game.setLettersUsed(game.getLettersUsed().concat(playersGuess)); //lisab kasutatud t�he
				updateCurrentWord(game, biggestFamPatt, playersGuess); //lisab �raarvatud t�hed
				return true;
			}
		}
		
		game.setWords(families.get(biggestFamPatt)); //uuendab s�nade seti
		game.setLettersUsed(game.getLettersUsed().concat(playersGuess)); //lisab kasutatud t�he
		game.subtractGuess();
		return false;
	}

	private static HashMap<String, HashSet<String>> divideIntoFamilies(
			String playersGuess, Game game) {
		
		//leiab k�ik perekonnad
		//lisab nendesse k�ik vastavad s�nad
		
		//v�tab iga s�na, teeb vastava mustri,
		//kui muster on mapis, lisab s�na, muidu v�tme ja s�na
		
		HashMap<String, HashSet<String>> families = new HashMap<String, HashSet<String>>();
		
		Iterator<String> wordSetIterator = game.getWords().iterator();
		
		while (wordSetIterator.hasNext()) {
			String word = wordSetIterator.next();
			String pattern = getPattern(word, playersGuess);
			
			if (families.containsKey(pattern)) {
				families.get(pattern).add(word);
			}
			else {
				HashSet<String> words = new HashSet<String>();
				words.add(word);
				families.put(pattern, words);
			}
		}
		return families;
	}

	private static String getPattern(String word, String playersGuess) {
		
		//makes a string that is a pattern of occurrences of the
		//given letter in the given word
		StringBuilder pattern = new StringBuilder();
		for (char c : word.toCharArray()) {
			if (c == playersGuess.charAt(0)) {
				pattern.append(playersGuess);
			}
			else {
				pattern.append("_");
			}
		}
		return pattern.toString();
	}

	//move to Utility or a class of it's own?
	private static HashSet<String> readWords(int WordLength) {
		
		Scanner failSisend;
		try {
			failSisend = new Scanner(new File("dictionary.txt"));
			HashSet<String> words = new HashSet<String>();
			
			while (failSisend.hasNextLine()) {
				String word = failSisend.nextLine(); 
				if (word.length() == WordLength) { //loeb ainult vajaliku pikkusega s�nad
					words.add(word);
				}
			}
			failSisend.close();
			return words;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return new HashSet<String>(); //et meetod alati returniks
		}
	}
	
	private static void updateCurrentWord(Game game, String pattern, String letter) {
		StringBuilder sb = new StringBuilder(game.getGuessedPart());
		
		for (int i = 0; i < pattern.length(); i++) {
			if (pattern.charAt(i) == letter.charAt(0)) {
				sb.replace(i, i+1, letter);
			}
		}
		game.setGuessedPart(sb.toString());
	}

	private static String findMax(HashMap<String, HashSet<String>> families) {
		
		int wordSetSize = 0;
		String pattern = "";
		
		for (Map.Entry<String, HashSet<String>> entry : families.entrySet()) {
			if (entry.getValue().size() > wordSetSize) {
				pattern = entry.getKey();
				wordSetSize = entry.getValue().size();
			}
		}
		return pattern;
	}
	
	private static String getRandWord(Game game) {
		int size = game.getWords().size();
		int randNr = new Random().nextInt(size);
		int i = 0;
		for (String word : game.getWords()) {
			if (i == randNr) {
				return word;
			}
			i = i +1;
		}
		return null;
	}
}
